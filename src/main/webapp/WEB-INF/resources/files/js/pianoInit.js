          var piano = Synth.createInstrument('piano');
            var firstNote = '';
            var goodNote = '';
            var ex = 0;



            $(document).ready(function() {
                $('#bex').attr("disabled", false);
                $('#repeatEx').hide();
                $('#validateEx').hide();
                $('#pop').hide();
                $('#end').hide();
                generateExercice(ex);
            });
            
            function generateExercice(ex)
            {
                if(ex===3)
                {
                    $('#exercice').hide();
                    $('#end').show(500);
                    
                }
                else
                {
                    $('#repeatEx').attr("disabled", false);
                    $('#validateEx').attr("disabled", false);
                    $('#repeatEx').hide();
                    $('#validateEx').hide();
                    $('#infoEx').hide();
                    $('#bex').show();
                }
            }

            
            
            function popBadFunc(pts)
            {
                $('#pop').html("Bad answer ! <br/> You've lost "+(pts*(-1))+" points.")
                $("#pop").show(200);
                setTimeout(function() {
                        $("#pop").hide(500);
                    }, 1300);
            }
            
            function popGoodFunc(pts)
            {
                $('#pop').html("Good answer ! <br/> You've won "+pts+" points.")
                $("#pop").show(200);
                setTimeout(function() {
                        $("#pop").hide(500);
                    }, 1300);
            }

            function addPoints(points)
            {
                $.ajax
                        ({
                            type: "POST",
                            url: "add.html",
                            data: {points: points},
                            success: function(response)
                            {
                                $('#points').html(response);
                            },
                            error: function(e) {
                                alert('Error');
                            }
                        });
            }

            function repeat()
            {
                piano.play(firstNote, 4, 2);
            }



