<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Silent Ways</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/modernizr.custom.25376.js" />"></script>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <div class="main clearfix">
                        <center>
                            <p><button id="showMenu">Menu</button></p>
                            <h1>${tuto.subject}    <it> by  ${tuto.authorName} </it></h1>
                            
                            <p>${tuto.content}</p>
                            
                        </center>

                    </div><!-- /main -->
                </div><!-- wrapper -->
            </div><!-- /container -->

            <nav class="outer-nav right vertical">
                <img style="margin-left: -70px; margin-bottom: 30px;" src="<c:url value="/resources/img/head.png" />"/>
                <sec:authorize ifAnyGranted="ROLE_USER, ROLE_ADMIN">
                    <a href="<c:url value="/index.html"/>" class="icon-home">Home</a>
                    <a href="<c:url value="/user/viewTuto.html"/>" class="icon-news">Tutorials</a>
                    <a href="<c:url value="/user/games/index"/>"class="icon-star">Games</a>
                    <a href="<c:url value="/user/index"/>"class="icon-star">Account</a>
                    <sec:authorize ifAnyGranted="ROLE_ADMIN">
                           <a href="<c:url value="/admin/viewUser.html"/>" class="icon-upload">Administration</a>
                    </sec:authorize>
                    <a href="<c:url value="/j_spring_security_logout" />" class="icon-lock">Logout</a>
                </sec:authorize>
            </nav>

        </center>
        <script src="<c:url value="/resources/js/menu.js" />"></script>
        <script src="<c:url value="/resources/js/classie.js" />"></script>
</body>
</html>
