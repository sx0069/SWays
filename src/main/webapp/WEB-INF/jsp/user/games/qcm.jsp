<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>SilentWays</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/modernizr.custom.25376.js" />"></script>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script> 
        <script src="<c:url value="/resources/js/qcm.js" />"></script>
        <script>
            function check()
            {
                tabGood = new Array();
                pts = 0;
                if($('input[name=g1]:checked', '#mcqForm').val()==${mcq.getMcq().get(0).ans})
                {
                    tabGood[0] = ${mcq.getMcq().get(0).points};
                    pts = pts + ${mcq.getMcq().get(0).points};
                }
                else
                {
                    tabGood[0] = (${mcq.getMcq().get(0).points})*(-1);
                    pts = pts - ${mcq.getMcq().get(0).points};
                }
                if($('input[name=g2]:checked', '#mcqForm').val()==${mcq.getMcq().get(1).ans})
                {
                    tabGood[1] = ${mcq.getMcq().get(1).points};
                    pts = pts + ${mcq.getMcq().get(1).points};
                }
                else
                {
                    tabGood[1] = (${mcq.getMcq().get(1).points})*(-1);
                    pts = pts - ${mcq.getMcq().get(1).points};
                }
                if($('input[name=g3]:checked', '#mcqForm').val()==${mcq.getMcq().get(2).ans})
                {
                    tabGood[2] = ${mcq.getMcq().get(2).points};
                    pts = pts + ${mcq.getMcq().get(2).points};
                }
                else
                {
                    tabGood[2] = (${mcq.getMcq().get(2).points})*(-1);
                    pts = pts - ${mcq.getMcq().get(2).points};
                }
                if($('input[name=g4]:checked', '#mcqForm').val()==${mcq.getMcq().get(3).ans})
                {
                    tabGood[3] = ${mcq.getMcq().get(3).points};
                    pts = pts + ${mcq.getMcq().get(3).points};
                }
                else
                {
                    tabGood[3] = (${mcq.getMcq().get(3).points})*(-1);
                    pts = pts - ${mcq.getMcq().get(3).points};
                }
                if($('input[name=g5]:checked', '#mcqForm').val()==${mcq.getMcq().get(4).ans})
                {
                    tabGood[4] = ${mcq.getMcq().get(4).points};
                    pts = pts + ${mcq.getMcq().get(4).points};
                }
                else
                {
                    tabGood[4] = (${mcq.getMcq().get(4).points})*(-1);
                    pts = pts - ${mcq.getMcq().get(4).points};
                }
                addPoints(pts);
                showGoodAns(tabGood, pts);
            }         
            function showGoodAns(tabGood, pts)
            {
                $('input:radio[name=g1]', '#mcqForm')[parseInt(${mcq.getMcq().get(0).ans})-1].checked = true;
                $('input:radio[name=g2]', '#mcqForm')[parseInt(${mcq.getMcq().get(1).ans})-1].checked = true;
                $('input:radio[name=g3]', '#mcqForm')[parseInt(${mcq.getMcq().get(2).ans})-1].checked = true;
                $('input:radio[name=g4]', '#mcqForm')[parseInt(${mcq.getMcq().get(3).ans})-1].checked = true;
                $('input:radio[name=g5]', '#mcqForm')[parseInt(${mcq.getMcq().get(4).ans})-1].checked = true;
                $('input:radio[name=g1]').attr("disabled",true);
                $('input:radio[name=g2]').attr("disabled",true);
                $('input:radio[name=g3]').attr("disabled",true);
                $('input:radio[name=g4]').attr("disabled",true);
                $('input:radio[name=g5]').attr("disabled",true);
                $('#add').hide();
                $('#home').show();
                i=1;
                while(i<6)
                {
                    $("#pts"+i).html(tabGood[i-1]+" Points");
                    $("#pts"+i).show();
                    if(tabGood[i-1]<0)
                    {
                        $('#pts'+i).css('background-color', 'rgba(220,110,110,0.78');
                        $('#pts'+i).css('color', 'white');
                    }
                    $('#pts'+i).css({
                        'padding' : '8px',
                        'width' : '200px',
                        'box-shadow' : '2px 2px 2px rgba(10,10,10,0.5)',
                        'margin-left' : '25px'
                     });
                    i++;
                }
                if(pts>0)
                    $('#won').html("You've won "+pts+" points !");
                else
                    $('#won').html("You've lost "+pts*(-1)+" points !");
                $('#won').show();
            }
         </script>
    </head>
    <body>
        
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <div class="main clearfix">
                        <center>
                            <p><button id="showMenu">Menu</button></p>
                            <h1>MCQ</h1>  
                            <div class="infoUser">
                                <h4><img src="${user.img}" alt="userImg"/><br/><br/>
                                Points : <span id="points">${user.points}</span><br/>
                            </div>


                            <form name="mcq" action="/qcmCheck" method="POST" modelAttribute="mcq" id="mcqForm">
                                <div >
                                    <br>
                                    <b>${mcq.getMcq().get(0).question}</b> <span id="pts1"></span>
                                    <br/><br/>
                                    <input type="radio" name="g1" value="1"> ${mcq.getMcq().get(0).ans1}
                                    <input type="radio" name="g1" value="2"> ${mcq.getMcq().get(0).ans2}
                                    <input type="radio" name="g1" value="3"> ${mcq.getMcq().get(0).ans3}
                                    <br/>
                                    <br/><br/>
                                    
                                    <b>${mcq.getMcq().get(1).question}</b> <span id="pts2"></span>
                                    <br/><br/>
                                    <input type="radio" name="g2" value="1"> ${mcq.getMcq().get(1).ans1}
                                    <input type="radio" name="g2" value="2">${mcq.getMcq().get(1).ans2}
                                    <input type="radio" name="g2" value="3">${mcq.getMcq().get(1).ans3}
                                    <br/>
                                    <br/><br/>
                                    
                                    <b>${mcq.getMcq().get(2).question}</b> <span id="pts3"></span>
                                    <br/><br/>
                                    <input type="radio" name="g3" value="1"> ${mcq.getMcq().get(2).ans1}
                                    <input type="radio" name="g3" value="2">${mcq.getMcq().get(2).ans2}
                                    <input type="radio" name="g3" value="3">${mcq.getMcq().get(2).ans3}
                                    <br/>
                                    <br/><br/>  
                                    <b>${mcq.getMcq().get(3).question}</b> <span id="pts4"></span>
                                    <br/><br/>
                                    <input type="radio" name="g4" value="1"> ${mcq.getMcq().get(3).ans1} 
                                    <input type="radio" name="g4" value="2"> ${mcq.getMcq().get(3).ans2}
                                    <input type="radio" name="g4" value="3"> ${mcq.getMcq().get(3).ans3}
                                    <br/>
                                    
                                    <br/><br/>
                                    <b>${mcq.getMcq().get(4).question}</b> <span id="pts5"></span>
                                    <br/><br/>
                                    <input type="radio" name="g5" value="1"> ${mcq.getMcq().get(4).ans1}
                                    <input type="radio" name="g5" value="2">${mcq.getMcq().get(4).ans2}
                                    <input type="radio" name="g5" value="3">${mcq.getMcq().get(4).ans3}
                                    <br/>
                                    <br/><br/>
                                    <button type="button" id="add" class="btn btn-info" onclick="check()">Send</button>
                                </div>
                            </form>
                            <h3><span id="won"></span></h3>
                            <a href="<c:url value="/user/games/index.html"/>" id="home"><button>Back to games</button></a>
                        </center>

                    </div><!-- /main -->
                </div><!-- wrapper -->
            </div><!-- /container -->

            <nav class="outer-nav right vertical">
                <img style="margin-left: -70px; margin-bottom: 30px;" src="<c:url value="/resources/img/head.png" />"/>
                <sec:authorize ifAnyGranted="ROLE_USER, ROLE_ADMIN">
                    <a href="<c:url value="/index.html"/>" class="icon-home">Home</a>
                        <a href="<c:url value="/user/viewTuto.html"/>" class="icon-news">Tutorials</a>
                    <a href="<c:url value="/user/games/index"/>" class="icon-news">Games</a>
                    <a href="<c:url value="/user/index"/>" class="icon-star">Account</a>
                    <sec:authorize ifAnyGranted="ROLE_ADMIN">
                        <a href="<c:url value="/admin/viewUser"/>" class="icon-upload">Administration</a>
                    </sec:authorize>

                    <a href="<c:url value="/j_spring_security_logout" />" class="icon-lock">Logout</a>
                </sec:authorize>
            </nav>
                </div>
        </center>
        <script src="<c:url value="/resources/js/menu.js" />"></script>
        <script src="<c:url value="/resources/js/classie.js" />"></script>
</body>
</html>
