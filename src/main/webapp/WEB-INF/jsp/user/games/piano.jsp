<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SilentWays</title>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/piano.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/modernizr.custom.25376.js" />"></script>
        <script src="<c:url value="/resources/js/audiosynth.js" />"></script>
        <script src="<c:url value="/resources/js/audiosynthView.js" />"></script>
        <script type="text/javascript">
            function validate()
            {
                var note = document.getElementById("info").innerHTML;

                if (ex === 0)
                {
                    if (goodNote === note)
                    {
                        addPoints(${keys.get(0).win});
                        ex++;
                        generateExercice(0);
                        popGoodFunc(${keys.get(0).win});
                    }
                    else
                    {
                        addPoints(${keys.get(0).loose});
                        popBadFunc(${keys.get(0).loose});
                    }
                }
                else if (ex === 1)
                {
                    if (goodNote === note)
                    {
                        addPoints(${keys.get(1).win});
                        ex++;
                        generateExercice(1);
                        popGoodFunc(${keys.get(1).win});
                    }
                    else
                    {
                        addPoints(${keys.get(1).loose});
                        popBadFunc(${keys.get(1).loose});

                    }
                }
                else if (ex === 2)
                {
                    if (goodNote === note)
                    {
                        addPoints(${keys.get(2).win});
                        ex++;
                        generateExercice(2);
                        popGoodFunc(${keys.get(2).win});
                    }
                    else
                    {
                        addPoints(${keys.get(2).loose});
                        popBadFunc(${keys.get(2).loose});
                    }
                }
                else if (ex === 3)
                {
                    if (goodNote === note)
                    {
                        addPoints(${keys.get(3).win});
                        ex++;
                        generateExercice(3);
                        popGoodFunc(${keys.get(3).win});
                    }
                    else
                    {
                        addPoints(${keys.get(3).loose});
                        popBadFunc(${keys.get(3).loose});
                    }
                }

            }

            function exercice()
            {
                //Charger exo aléatoire dans l'interval
                $('#bex').hide();
                $('#repeatEx').show();
                $('#validateEx').show();
                $('#infoEx').show();
                if (ex === 0)
                {
                    firstNote = '${keys.get(0).noteD}';
                    goodNote = '${keys.get(0).noteA}';
                    $('#infoEx').html("${keys.get(0).question}");
                }
                else if (ex === 1)
                {
                    firstNote = '${keys.get(1).noteD}';
                    goodNote = '${keys.get(1).noteA}';
                    $('#infoEx').html("${keys.get(1).question}");
                }
                else if (ex === 2)
                {
                    firstNote = '${keys.get(2).noteD}';
                    goodNote = '${keys.get(2).noteA}';
                    $('#infoEx').html("${keys.get(2).question}");
                }
                else if (ex === 3)
                {
                    firstNote = '${keys.get(3).noteD}';
                    goodNote = '${keys.get(3).noteA}';
                    $('#infoEx').html("${keys.get(3).question}");
                }
            }
        </script>

    </head>
    <body>
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <div class="main clearfix">     
                        <center>
                            <p><button id="showMenu">Menu</button></p>
                            <h1>Interval exercices</h1>
                            <div class="infoUser">
                                <h4><img src="${user.img}" alt="userImg"/><br/><br/>
                                Points : <span id="points">${user.points}</span><br/>
                            </div>

                            <h2>Note : <span id="info">&nbsp;</span></h2>
                            <div ID="keyboard" class="keyboard-holder"></div>
                            <div id="end"><h2>Congratulations, you've finished the exercices</h2>
                                <a href="<c:url value="/user/games/index.html"/>"><button>Back to games</button></a></div>
                            <div id="exercice">

                                <button id="bex" onclick="exercice()">Play</button>
                                <h4><div id="infoEx"></div></h4>
                                <button id="repeatEx" onclick="repeat()">Play the Note</button><br/>

                                <button id="validateEx" onclick="validate()">Validate</button>
                                <center><h5 id="pop" style="border-radius: 4px; box-shadow: 2px 2px 6px rgba(10,10,10,0.6); padding:6px; width: 150px; left:auto; "></h5></center>
                            </div>


                    </div><!-- /main -->
                </div><!-- wrapper -->
            </div><!-- /container -->
            <nav class="outer-nav right vertical">
                <img style="margin-left: -70px; margin-bottom: 30px;" src="<c:url value="/resources/img/head.png" />"/>
                <sec:authorize ifAnyGranted="ROLE_USER, ROLE_ADMIN">
                    <a href="<c:url value="/index.html"/>" class="icon-home">Home</a>
                        <a href="<c:url value="/user/viewTuto.html"/>" class="icon-news">Tutorials</a>
                    <a href="<c:url value="/user/games/index.html"/>" class="icon-news">Games</a>
                    <a href="<c:url value="/user/index.html"/>" class="icon-star">Account</a>
                    <sec:authorize ifAnyGranted="ROLE_ADMIN">
                        <a href="<c:url value="/admin/viewUser.html"/>" class="icon-upload">Administration</a>
                    </sec:authorize>
                    <a href="<c:url value="/j_spring_security_logout" />" class="icon-lock">Logout</a>
                </sec:authorize>
            </nav>
        </div>
        <script src="<c:url value="/resources/js/menu.js" />"></script>
        <script src="<c:url value="/resources/js/classie.js" />"></script>
        <script src="<c:url value="/resources/js/pianoInit.js" />"></script>
        <script type="text/javascript">
            var a = new AudioSynthView();
            a.draw();
        </script>
    </body>
</html>
