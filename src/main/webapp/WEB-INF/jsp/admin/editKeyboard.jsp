<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script type="text/javascript" src="<c:url value="/resources/ckeditor/ckeditor.js" />"></script>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Silent Ways</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/modernizr.custom.25376.js" />"></script>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
    </head>
    <body>
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <div class="main clearfix">
                        <center>
                            <p><button id="showMenu">Menu</button></p>
                            <h1>Edition of Keyboard</h1>

                            <c:url var="viewUsrUrl" value="/admin/viewUser.html" />
                            <h1>Update Keyboard </h1>
                            <c:if test="${not empty error}">
                                <div class="alert alert-danger">
                                    Your edit attempt was not successful, try again.<br /> Caused by :
                                    ${error}
                                </div>
                            </c:if>
                            <br />

                            <c:url var="UpdateKeyboardUrl" value="/admin/updateKeyboard/${keyb.id}/" />
                            <form:form role="form" modelAttribute="keyb" method="POST" action="${UpdateKeyboardUrl}">
                                <div class="form-group">
                                    <form:label path="question">Question</form:label><br/>
                                    <form:input path="question" />
                                </div>

                                <div class="form-group">
                                    <form:label path="noteA">Note Arriv�e</form:label><br/>
                                    <form:input path="noteA" />
                                </div>

                                <div class="form-group">
                                    <form:label path="noteD">Note D�part</form:label><br/>
                                    <form:input path="noteD" />
                                </div>

                                <div class="form-group">
                                    <form:label path="win">Point Win</form:label><br/>
                                    <form:input path="win" />
                                </div>

                                <div class="form-group">
                                    <form:label path="loose">Point Loose</form:label><br/>
                                    <form:input path="loose" />
                                </div>

                                <br />
                                <br/>

                                <button type="submit" class="btn btn-default">Update Keyboard</button><br/><br/>

                            </form:form>
                            <a href="${viewUsrUrl}"><button class="btn btn-primary">Go Back</button></a>
                            <br/>
                        </center>

                    </div><!-- /main -->
                </div><!-- wrapper -->
            </div><!-- /container -->

            <nav class="outer-nav right vertical">
                <img style="margin-left: -70px; margin-bottom: 30px;" src="<c:url value="/resources/img/head.png" />"/>
                <sec:authorize ifAnyGranted="ROLE_USER, ROLE_ADMIN">
                    <a href="<c:url value="/index.html"/>" class="icon-home">Home</a>
                    <a href="<c:url value="/user/viewTuto.html"/>" class="icon-news">Tutorials</a>
                    <a href="<c:url value="/user/games/index.html"/>" class="icon-star">Games</a>
                    <a href="<c:url value="/user/index.html"/>" class="icon-star">Account</a>
                    <sec:authorize ifAnyGranted="ROLE_ADMIN">
                        <a href="<c:url value="/user/viewUser.html"/>" class="icon-upload">Administration</a>
                    </sec:authorize>
                    <a href="<c:url value="/j_spring_security_logout" />" class="icon-lock">Logout</a>
                </sec:authorize>
            </nav> 

        </center>
        <script src="<c:url value="/resources/js/menu.js" />"></script>
        <script src="<c:url value="/resources/js/classie.js" />"></script>
</body>
</html>
