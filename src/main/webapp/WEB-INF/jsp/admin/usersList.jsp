<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>

    <head>

        <title>Silent Ways</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/css/menu.css" />" rel="stylesheet">
        <script src="<c:url value="/resources/js/modernizr.custom.25376.js" />"></script>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script>
            $(document).ready(function() {
                $("#questionDiv").hide();
                $("#keyboardDiv").hide();
                $("#usersDiv").hide();
                $("#tutoDiv").hide();
            });
            function showUsers()
            {
                $("#questionDiv").hide();
                $("#keyboardDiv").hide();
                $("#usersDiv").show(500);
                $("#tutoDiv").hide();
            }
            function showQuestion()
            {
                $("#questionDiv").show(500);
                $("#keyboardDiv").hide();
                $("#usersDiv").hide();
                $("#tutoDiv").hide();
            }
            function showKeyboard()
            {
                $("#questionDiv").hide();
                $("#keyboardDiv").show(500);
                $("#usersDiv").hide();
                $("#tutoDiv").hide();
            }

            function showTuto()
            {
                $("#questionDiv").hide();
                $("#keyboardDiv").hide();
                $("#usersDiv").hide();
                $("#tutoDiv").show(500);
            }
        </script>
    </head>

    <body>
        <div id="perspective" class="perspective effect-moveleft">
            <div class="container">
                <div class="wrapper"><!-- wrapper needed for scroll -->
                    <div class="main clearfix">     
                        <center>
                            <p><button id="showMenu">Menu</button></p>
                            <button onclick="showUsers();">Users</button>
                            <button onclick="showQuestion();">Questions</button>
                            <button onclick="showKeyboard();">Keyboard</button>
                            <button onclick="showTuto();">Tutorial</button>
                            <div id="usersDiv">
                                <h1>List of Users</h1><br/>

                                <c:if test="${!empty users}">

                                    <table class="table table-hover">

                                        <th><b>ID</b></th>
                                        <th><b>Pseudo</b></th>
                                        <th><b>Pass</b></th>
                                        <th><b>Name</b></th>
                                        <th><b>Surname</b></th>
                                        <th><b>Registration Date</b></th>
                                        <th><b>Points</b></th>
                                        <th><b>Mail</b></th>
                                        <th><b>Img</b></th>
                                        <th><b>Actions</b></th>

                                        <c:forEach items="${users}" var="user">

                                            <tr>
                                            <th><c:out value="${user.id}"/></th>
                                            <th><c:out value="${user.pseudo}"/></th>
                                            <th><c:out value="${user.password}"/></th>
                                            <th><c:out value="${user.name}"/></th>
                                            <th><c:out value="${user.surname}"/></th>
                                            <th><c:out value="${user.registrationDate}"/></th>
                                            <th><c:out value="${user.points}"/></th>
                                            <th><c:out value="${user.mailAddress}"/></th>
                                            <th><img width="40" height="40" alt="userIcon" src="${user.img}" class="img-thumbnail"/></th>
                                            <th><a href="deleteUser/${user.id}/"><button type="button" class="btn btn-default">Delete</button></a></th>
                                            <th><c:if test="${user.enabled == true}">ACTIVATED</c:if><c:if test="${user.enabled == false}">NOT ACTIVATED</c:if></th>

                                                </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <br/><br/><br/>
                            </div>
                            <div id="questionDiv">
                                <h1>List of Questions</h1><br/>

                                <c:if test="${!empty questions}">

                                    <table class="table table-hover">

                                        <th><b>Question</b></th>
                                        <th><b>Answer 1</b></th>
                                        <th><b>Answer 2</b></th>
                                        <th><b>Answer 2</b></th>
                                        <th><b>Right Answer</b></th>
                                        <th><b>Points</b></th>

                                        <c:forEach items="${questions}" var="question">
                                            <tr>
                                            <th><c:out value="${question.question}"/></th>
                                            <th><c:out value="${question.ans1}"/></th>
                                            <th><c:out value="${question.ans2}"/></th>
                                            <th><c:out value="${question.ans3}"/></th>
                                            <th><c:out value="${question.ans}"/></th>
                                            <th><c:out value="${question.points}"/></th>

                                            <th><a href="updateQuestion/${question.id}/"><button type="button" class="btn btn-default">Update</button></a></th>
                                            <th><a href="deleteQuestion/${question.id}/"><button type="button" class="btn btn-default">Delete</button></a></th>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <table>
                                    <th><a href="addQuestion.html"><button type="button" class="btn btn-default">Add Question</button></a></th>
                                </table>

                                <br/><br/><br/>
                            </div>
                            <div id="keyboardDiv">
                                <h1>List of Keyboard's Question</h1><br/>

                                <c:if test="${!empty keyboards}">

                                    <table class="table table-hover">

                                        <th><b>Question</b></th>
                                        <th><b>note asked</b></th>
                                        <th><b>note answer</b></th>
                                        <th><b>win</b></th>
                                        <th><b>Loose</b></th>

                                        <c:forEach items="${keyboards}" var="keyb" >
                                            <tr>
                                            <th><c:out value="${keyb.question}"/></th>
                                            <th><c:out value="${keyb.noteA}"/></th>
                                            <th><c:out value="${keyb.noteD}"/></th>
                                            <th><c:out value="${keyb.win}"/></th>
                                            <th><c:out value="${keyb.loose}"/></th>

                                            <th><a href="updateKeyboard/${keyb.id}/"><button type="button" class="btn btn-default">Update</button></a></th>
                                            <th><a href="deleteKeyboard/${keyb.id}/"><button type="button" class="btn btn-default">Delete</button></a></th>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <table>
                                    <th><a href="addKeyboard.html"><button type="button" class="btn btn-default">Add Keyboard</button></a></th>
                                </table>
                            </div>

                            <div id="tutoDiv">
                                <h1>List of Tutorials</h1>

                                <c:if test="${!empty tutos}">

                                    <table class="table table-hover">
                                        <tr>
                                        <th><b>ID</b></th>
                                        <th><b>Subject</b></th>
                                        <th><b>Author Name</b></th>
                                        </tr>
                                        <c:forEach items="${tutos}" var="tuto">
                                            <tr>
                                            <th><c:out value="${tuto.id}"/></th>
                                            <th><c:out value="${tuto.subject}"/></th>
                                            <th><c:out value="${tuto.authorName}"/></th>

                                            <th><a href="updateTuto/${tuto.id}/"><button type="button" class="btn btn-default">Update</button></a></th>
                                            <th><a href="deleteTuto/${tuto.id}/"><button type="button" class="btn btn-default">Delete</button></a></th>
                                            </tr>  
                                        </c:forEach>
                                    </table>
                                </c:if>

                                <table>
                                    <th><a href="addTuto.html"><button type="button" class="btn btn-default">Add Tutorial</button></a></th>
                                </table>
                                <br/><br/>
                            </div>

                        </center>
                    </div><!-- /main -->
                </div><!-- wrapper -->
            </div><!-- /container -->
            <nav class="outer-nav right vertical">
                <img style="margin-left: -70px; margin-bottom: 30px;" src="<c:url value="/resources/img/head.png" />"/>
                <sec:authorize ifAnyGranted="ROLE_USER, ROLE_ADMIN">
                    <a href="<c:url value="/index.html"/>" class="icon-home">Home</a>
                    <a href="<c:url value="/user/viewTuto.html"/>" class="icon-news">Tutorials</a>
                    <a href="<c:url value="/user/games/index.html"/>" class="icon-star">Games</a>
                    <a href="<c:url value="/user/index"/>" class="icon-star">Account</a>  
                    <a href="<c:url value="/j_spring_security_logout" />" class="icon-lock">Logout</a>
                </sec:authorize>
            </nav> 
        </div>
        <script src="<c:url value="/resources/js/menu.js" />"></script>
        <script src="<c:url value="/resources/js/classie.js" />"></script>
    </body>

</html>
