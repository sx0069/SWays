package com.dao;

import com.entities.Keyboard;
import java.util.List;

public interface KeyboardDao 
{
    public List<Keyboard> listKeyboard();
    
    public void update(Keyboard k);
    
    public Keyboard get(int id);
    
    public void add(Keyboard k);
    
    public void delete(Keyboard k);
    
    public void deleteKeyboard(int id);
    
    public Integer getNb();
    
    public List<Keyboard> listeKeyboard(int nb);
    
    public List<Integer> getListofId();
    
    //public List<Keyboard> liste();
    
}
