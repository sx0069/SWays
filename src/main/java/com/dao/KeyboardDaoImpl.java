package com.dao;

import com.entities.Keyboard;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.hibernate.Query;
import org.hibernate.SQLQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class KeyboardDaoImpl implements KeyboardDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void delete(Keyboard k) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(k);
    }

    @Transactional
    @Override
    public List<Keyboard> listKeyboard() {
        Session session = sessionFactory.getCurrentSession();
        List keyboards = session.createQuery("from com.entities.Keyboard").list();
        return keyboards;
    }

    @Transactional
    @Override
    public void update(Keyboard k) {
        Session session = sessionFactory.getCurrentSession();
        session.update(k);
    }

    @Transactional
    @Override
    public Keyboard get(int id) {
        Session session = sessionFactory.getCurrentSession();
        Keyboard k = (Keyboard) session.createCriteria(Keyboard.class).add(Restrictions.eq("id", id)).uniqueResult();
        return k;
    }

    @Transactional
    @Override
    public void add(Keyboard k) {
        Session session = sessionFactory.getCurrentSession();
        session.save(k);
    }

    @Transactional
    @Override
    public Integer getNb() {
        Session session = sessionFactory.getCurrentSession();
        SQLQuery query = session.createSQLQuery("SELECT count(id) FROM mydb.game_keyboard");
        BigInteger result = (BigInteger) query.uniqueResult();
        return result.intValue();
    }

    @Transactional
    @Override
    public void deleteKeyboard(int id) {
        Session session = sessionFactory.getCurrentSession();
        Keyboard keyb = (Keyboard) sessionFactory.getCurrentSession().load(Keyboard.class, id);
        if (null != keyb) {
            session.delete(keyb);
        }

    }
/*
    @Transactional
    @Override
    public List<Keyboard> liste() {
        List<Keyboard> listeK = new ArrayList<Keyboard>();
        int i = 0, j = 0;
        boolean ok;
        Random rand = new Random();
        List<Integer> list = new ArrayList();
        int nb = this.getNb();

        while (i < 4) {
            int nombre = rand.nextInt(nb);
            ok = true;
            for (Integer it : list) {
                if (it == nombre) {
                    ok = false;
                }
            }
            if (ok == true) {
                listeK.add(this.get(nombre + 1));
                list.add(nombre);
                i++;
            }

        }
        return listeK;
    } */

     @Transactional
    @Override
    public List<Keyboard> listeKeyboard(int nb) {
        int i = 0;
        List<Keyboard> listeK = new ArrayList<Keyboard>();
        
        List<Integer> listOfId = this.getListofId();
        Collections.shuffle(listOfId);
        List<Integer> usedId = new ArrayList();
        while(i<nb)
        {
            for(Integer it : listOfId)
            {
                if(i<nb)
                {
                    if(!usedId.contains(it))
                    {
                        listeK.add(this.get(it));
                        usedId.add(it);
                        i++;
                    }
                    Collections.shuffle(listOfId);
                    
                }
            }
        }
        return listeK;
    }

     @Transactional
    @Override
    public List<Integer> getListofId()
    {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("from com.entities.Keyboard");
        List<Keyboard> list = query.list();
        List<Integer> listOfId = new ArrayList();
        for(Keyboard it : list)
        {
            listOfId.add(it.getId());
        }
        return listOfId;
    }
}
