package com.controller;

import com.entities.Keyboard;
import com.entities.Question;
import com.entities.User;
import com.method.UserMethod;
import com.service.KeyboardService;
import com.service.KeyboardServiceImpl;
import com.service.QuestionService;
import com.service.TutorialService;
import com.service.UserService;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller("adminController")
@RequestMapping("/admin")
public class AdminController 
{
    @Autowired
    private UserService userService;
    
    @Autowired
    private TutorialService tutoService;
    
     @Autowired
     private QuestionService questionService;
     
      @Autowired
     private KeyboardService keyboardService;

    @RequestMapping(value="/index", method = RequestMethod.GET)
    public ModelAndView index() 
    {
        return new ModelAndView("admin/index");
    }

    
    //Affichage des utilisateurs
    @RequestMapping(value="/viewUser", method = RequestMethod.GET)
    public ModelAndView listArticles() 
    {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("users",  userService.userList());
        model.put("questions", questionService.questionList());
        model.put("keyboards", keyboardService.listKeyboard());
        model.put("tutos", tutoService.tutorialList());
        ModelAndView mav = new ModelAndView("admin/usersList", model);
        return mav;
    }
    
    //Suppression d'un utilisateur
    @RequestMapping(value="/deleteUser/{userId}/")
    public String deleteContact(@PathVariable Integer userId) 
    {
        userService.deleteUser(userId);
        return "redirect:/admin/viewUser.html";
    }
    
    //Suppression d'un tutoriel
    @RequestMapping(value="/deleteTuto/{tutoId}/")
    public String deleteTuto(@PathVariable Integer tutoId) 
    {
        tutoService.deleteTutorial(tutoId);
        return "redirect:/admin/viewUser.html";
    }
    
    //Suppression d'un keyboard
    @RequestMapping(value="/deleteKeyboard/{keyboardId}/")
    public String deleteKeyboard(@PathVariable Integer keyboardId) 
    {
        if(keyboardService.getNb()>5){
            keyboardService.deleteKeyboard(keyboardId);
        }
        return "redirect:/admin/viewUser.html";
    }
    
        //Suppression d'une question
    @RequestMapping(value="/deleteQuestion/{questionId}/")
    public String deleteQuestion(@PathVariable Integer questionId) 
    {
        if(questionService.getNbQuestion()>5){
            questionService.deleteQuestion(questionId);    
        }
        return "redirect:/admin/viewUser.html";
    }
    
     //Formulaire de MAJ de question
    @RequestMapping(value="/updateQuestion/{questionId}/", method=RequestMethod.GET)
    public ModelAndView edit(@PathVariable Integer questionId){
        Question quest = questionService.getQuestionObj(questionId);      
        ModelAndView mav = new ModelAndView("admin/editQuestion", "quest", quest);
        return mav;
    }
    
    // MAJ de question
    @RequestMapping(value="/updateQuestion/{questionId}/", method=RequestMethod.POST)
    public ModelAndView updateQuestion(@ModelAttribute("question") Question quest, BindingResult result, @PathVariable Integer questionId){
        quest.setId(questionId);
        questionService.updateQuestion(quest);
        return new ModelAndView("redirect:/admin/viewUser.html");
    }
    
    //ajout d'une question
    @RequestMapping(value = "/addQuestion", method = RequestMethod.POST)
    public ModelAndView addQuest(@ModelAttribute(value = "question") Question quest, BindingResult result) {
            questionService.addQuestion(quest);
            ModelAndView mav = new ModelAndView("redirect:/admin/viewUser.html");
            return mav;
    }
    
    //formulaire d'ajout d'une question
    @RequestMapping(value = "/addQuestion", method = RequestMethod.GET)
    public ModelAndView addQuestion(@ModelAttribute(value = "question") Question quest, BindingResult result) {
        ModelAndView mav = new ModelAndView("admin/addQuestion");
        return mav;
    }
    
    
    //Formulaire de MAJ de keyboard
    @RequestMapping(value="/updateKeyboard/{keyboardId}/", method=RequestMethod.GET)
    public ModelAndView editKeyboard(@PathVariable Integer keyboardId){
        Keyboard keyb = keyboardService.get(keyboardId);      
        ModelAndView mav = new ModelAndView("admin/editKeyboard", "keyb", keyb);
        return mav;
    }
    
    // MAJ de keyboard
    @RequestMapping(value="/updateKeyboard/{keyboardId}/", method=RequestMethod.POST)
    public ModelAndView updateKeyboard(@ModelAttribute("keyboard") Keyboard keyb, BindingResult result, @PathVariable Integer keyboardId){
        keyb.setId(keyboardId);
        keyboardService.update(keyb);
        return new ModelAndView("redirect:/admin/viewUser.html");
    }
    
        //ajout d'une keyboard
    @RequestMapping(value = "/addKeyboard", method = RequestMethod.POST)
    public ModelAndView addKeyb(@ModelAttribute(value = "keyboard") Keyboard keyb, BindingResult result) {
            keyboardService.add(keyb);
            ModelAndView mav = new ModelAndView("redirect:/admin/viewUser.html");
            return mav;
    }
    
    //formulaire d'ajout d'un keyboard
    @RequestMapping(value = "/addKeyboard", method = RequestMethod.GET)
    public ModelAndView addKeyboard(@ModelAttribute(value = "keyboard") Keyboard keyb, BindingResult result) {
        ModelAndView mav = new ModelAndView("admin/addKeyboard");
        return mav;
    }
 

}
