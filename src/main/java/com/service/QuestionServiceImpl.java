package com.service;

import com.dao.QuestionDao;
import com.entities.Question;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("questionService")
@Transactional(propagation = Propagation.SUPPORTS)
public class QuestionServiceImpl implements QuestionService
{
    
    @Autowired
    QuestionDao questionDao;
    
    @Override
    public List<Question> questionList(){
        return questionDao.questionList();
    }
    
    @Override
    public Integer getNbQuestion(){
        return questionDao.getNbQuestion();
    }
    
    @Override
    public Question getQuestionObj(int id)
    {
        return questionDao.getQuestionObj(id);
    }
    
    @Override
    public String getQuestion(int id)
    {
        return questionDao.getQuestion(id);
    }
    
    @Override
    public String getAnswer1(int id)
    {
        return questionDao.getAnswer1(id);
    }
    
    @Override
    public String getAnswer2(int id)
    {
        return questionDao.getAnswer2(id);
    }    
    
    @Override
    public String getAnswer3(int id)
    {
        return questionDao.getAnswer3(id);
    }

    
    @Override
    public void setQuestion(int id,String qu)
    {
        questionDao.setQuestion(id,qu);
    }  
    
    @Override
    public void setAnswer1(int id,String an)
    {
        questionDao.setAnswer1(id,an);
    } 
    
    @Override
    public void setAnswer2(int id,String an)
    {
        questionDao.setAnswer2(id,an);
    }  
    
    @Override
    public void setAnswer3(int id,String an)
    {
        questionDao.setAnswer3(id,an);
    }   
    
    @Override
    public int getGoodAns(int id)
    {
        return questionDao.getGoodAns(id);
    }
    
    @Override
    public void updateQuestion(Question quest){
        questionDao.updateQuestion(quest);
    }

    @Override
    public void deleteQuestion(int id){
        questionDao.deleteQuestion(id);
    }

    @Override
    public void addQuestion(Question quest){
        questionDao.addQuestion(quest);
    }
}
