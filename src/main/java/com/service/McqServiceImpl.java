package com.service;


import java.util.Random;
import com.dao.QuestionDao;
import com.entities.Mcq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("mcqService")
@Transactional(propagation = Propagation.SUPPORTS)
public class McqServiceImpl implements McqService
{
    @Autowired
    QuestionDao questionDao;

    @Override
    public Mcq generateMcq(int nb) {
        int i = 0;
        Mcq mcq = new Mcq();
        
        List<Integer> listOfId = questionDao.getListofId();
        Collections.shuffle(listOfId);
        List<Integer> usedId = new ArrayList();
        while(i<nb)
        {
            for(Integer it : listOfId)
            {
                if(i<nb)
                {
                    if(!usedId.contains(it))
                    {
                        mcq.addQuestion(questionDao.getQuestionObj(it));
                        usedId.add(it);
                        i++;
                    }
                    Collections.shuffle(listOfId);
                    
                }
            }
        }
        return mcq;
    }
    
    
}
