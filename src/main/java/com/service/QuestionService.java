package com.service;

import com.entities.Question;
import java.util.List;

public interface QuestionService {

    public List<Question> questionList();

    public Question getQuestionObj(int id);

    public String getQuestion(int id);

    public String getAnswer1(int id);

    public String getAnswer2(int id);

    public String getAnswer3(int id);

    public void setQuestion(int id, String qu);

    public void setAnswer1(int id, String an);

    public void setAnswer2(int id, String an);

    public void setAnswer3(int id, String an);

    public int getGoodAns(int id);

    public void updateQuestion(Question quest);

    public void deleteQuestion(int id);

    public void addQuestion(Question quest);
    
    public Integer getNbQuestion();

}
