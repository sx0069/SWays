package com.service;

import com.dao.KeyboardDao;
import java.util.List;
import java.util.Random;

import com.dao.KeysDao;
import com.entities.Keyboard;
import com.entities.Keys;
import java.util.ArrayList;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("keyboardService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class KeyboardServiceImpl implements KeyboardService
{
    @Autowired
    KeyboardDao keyboardDao;
    
    @Override
    public List<Keyboard> listKeyboard(){
        return keyboardDao.listKeyboard();
    }
    
    @Override
    public List<Keyboard> liste(int nb){
        return keyboardDao.listeKeyboard(nb);
    }
    @Override
    public void update(Keyboard k)
    {
        keyboardDao.update(k);
    }
    
    @Override
    public Keyboard get(int id)
    {
        return keyboardDao.get(id);
    }
    
    @Override
    public void add(Keyboard k)
    {
        keyboardDao.add(k);
    }
    
    @Override
    public void delete(Keyboard k)
    {
        keyboardDao.delete(k);
    }
    
    @Override
    public Integer getNb()
    {
        return keyboardDao.getNb();
    }
    

    @Override
    public void deleteKeyboard(int id) {
       keyboardDao.deleteKeyboard(id);
    }
}
